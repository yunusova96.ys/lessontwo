package lessonTwo;

import java.util.Arrays;

public class LessonTwoArray {
    public static void main(String[] args) {
        int MAX = 10;
        int r = (int) (Math.random() * (MAX + 1));

        int[] a = new int[10];
        for (int i = 0; i < a.length; i++) {
            //fill it
            a[i] = i;
        }
        System.out.println(Arrays.toString(a));
    }
}